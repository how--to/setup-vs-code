# Install
Download VS Code at this address: [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

# Extensions
## Access
Go to `Code -> Preferences -> Extensions` (⇧⌘X)

## Install 
- DotEnv
- ESLint
- Docker
- GitLab Workflow

# User Settings
## Access
Go to `Code -> Preferences -> Settings` (⌘,)

## Example
```json
{
    "editor.tabSize": 2,
    "eslint.options": {
        "configFile": "../.eslintrc.json",
    },
    "eslint.autoFixOnSave": true,
    "eslint.alwaysShowStatus": true,
    "eslint.workingDirectories": [
        "./src",
    ],
    "files.autoSave": "onFocusChange",
}
```
